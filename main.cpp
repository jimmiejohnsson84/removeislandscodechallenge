#include <iostream>
#include <vector>
#include <set>

// Forward declare
struct Node;

struct Node
{
    std::vector<Node> m_nodes;
    int m_row;
    int m_col;
};

void islandsToKeepFromGraph(std::set<std::pair<int, int>> &islandsToSave, const Node &node)
{
    using namespace std;
    islandsToSave.insert(pair<int, int>(node.m_row, node.m_col));
    for(const auto child : node.m_nodes)
    {
        islandsToKeepFromGraph(islandsToSave, child);
    }
}

void initNode(Node &node)
{
    node.m_row = -1;
    node.m_col = -1;
}

void initNodes(std::vector<Node> &nodes)
{
    for(auto &node : nodes)
    {
        initNode(node);
    }
}
void visitChildren(const std::vector<std::vector<int>> &data, std::set<std::pair<int, int>> &visitedNodes, Node &node);

/*

        [
        [1, 0, 0, 0, 0, 0]
        [0, 1, 0, 1, 1, 1]
        [0, 0, 1, 0, 1, 0]
        [1, 1, 0, 0, 1, 0]
        [1, 0, 1, 1, 0, 0]
        [1, 0, 0, 0, 0, 1]
        ]

*/

void checkAndCreateNewNode(const std::vector<std::vector<int>> &data, std::set<std::pair<int, int>> &visitedNodes, Node &node, 
                            int rowOffset, int colOffset)
{
    using namespace std;

    pair<int, int> nodeToVisit(node.m_row + rowOffset, node.m_col + colOffset);
    if(visitedNodes.find(nodeToVisit) == visitedNodes.end())
    {
        if(node.m_row + rowOffset >= 0 && node.m_row + rowOffset < data.size() &&
            node.m_col + colOffset >= 0 && node.m_col + colOffset < data[0].size())
        {
            if(data[node.m_row + rowOffset][node.m_col + colOffset] == 1)
            {
                visitedNodes.insert(nodeToVisit);
               
                Node theNode;
                initNode(theNode);
                theNode.m_row = node.m_row + rowOffset;
                theNode.m_col = node.m_col + colOffset;
                
                // Add as child
                node.m_nodes.push_back(theNode);
                int currentNode = node.m_nodes.size() - 1;


                // recursively continue
                visitChildren(data, visitedNodes, node.m_nodes[currentNode]);
            }
        }
    }

}

/*
        [
        [1, 0, 0, 0, 0, 0]
        [0, 1, 0, 1, 1, 1]
        [0, 0, 1, 0, 1, 0]
        [1, 1, 0, 0, 1, 0]
        [1, 0, 1, 1, 0, 0]
        [1, 0, 0, 0, 0, 1]
        ]

*/

void visitChildren(const std::vector<std::vector<int>> &data, std::set<std::pair<int, int>> &visitedNodes, Node &node)
{
    using namespace std;

    const bool isTopRow = node.m_row == 0;
    const bool isBottomRow = node.m_row == data.size() - 1;
    const bool isLeftCol = node.m_col == 0;
    const bool isRightCol = node.m_col == data[0].size() - 1;

    //cout << "At: " << node.m_row << ", " << node.m_col << endl;

    if(!isTopRow)
    {       
        checkAndCreateNewNode(data, visitedNodes,  node, -1, 0);
    }
    if(!isBottomRow)
    {
        checkAndCreateNewNode(data, visitedNodes, node, 1, 0);
    }
    if(!isLeftCol)
    {
        checkAndCreateNewNode(data, visitedNodes, node, 0, -1);
    }
    if(!isRightCol)
    {
        checkAndCreateNewNode(data, visitedNodes, node, 0, 1);
    }
}

void createGraph(const std::vector<std::vector<int>> &data, Node &head)
{
    using namespace std;

    std::vector<int> leftFrame;
    std::vector<int> rightFrame;

    const std::vector<int> &topCols = data[0];
    const std::vector<int> &bottomCols = data[data.size() - 1];

    std::set<std::pair<int, int>> visitedNodes;

    // Create children, these are the borders
    
    // Start with left side (assume height of matrix is at least 1)
    for(int i = 1; i < data.size() - 1; i++)
    {
        if(data[i][0] == 1)
        {
            Node theNode;
            initNode(theNode);
            theNode.m_row = i;
            theNode.m_col = 0;
            head.m_nodes.push_back(theNode);
            visitedNodes.insert(pair<int, int>(theNode.m_row, theNode.m_col));
        }        
    }
    // Right side
    for(int i = 1; i < data.size() - 1; i++)
    {        
        const std::vector<int> cols = data[i];
        //cout << cols[cols.size() - 1] << ", " << endl;
        if(cols[cols.size() - 1] == 1)
        {
            Node theNode;
            initNode(theNode);
            theNode.m_row = i;
            theNode.m_col = cols.size() - 1;
            head.m_nodes.push_back(theNode);
            visitedNodes.insert(pair<int, int>(theNode.m_row, theNode.m_col));
        }
    }
    // Top
    for(int i = 0; i < topCols.size(); i++)
    {
        if(topCols[i] == 1)
        {
            Node theNode;
            initNode(theNode);
            theNode.m_row = 0;
            theNode.m_col = i;
            head.m_nodes.push_back(theNode);
            visitedNodes.insert(pair<int, int>(theNode.m_row, theNode.m_col));
        }
    }
    // Bottom
    for(int i = 0; i < bottomCols.size(); i++)
    {
        if(bottomCols[i] == 1)
        {
            Node theNode;
            initNode(theNode);
            theNode.m_row = data.size() - 1;
            theNode.m_col = i;
            head.m_nodes.push_back(theNode);
            visitedNodes.insert(pair<int, int>(theNode.m_row, theNode.m_col));
        }        
    }

    // Print our results
    /*
    for(auto nodeV : head.m_nodes)
    {
        cout << nodeV.m_value << ", " << endl;
    }
    */

   for(auto &node : head.m_nodes)
   {       
       visitChildren(data, visitedNodes, node);
   }

   // Print all visited nodes:
   /*
   for(auto visited : visitedNodes)
   {
       cout << "Visited: " << visited.first << ", " << visited.second << endl;
   }
   */
}


void printGraph(const Node &node);

void printGraphHead(const Node &head)
{
    using namespace std;

    for(auto child : head.m_nodes)
    {
        cout << "[ " << child.m_row << ", " << child.m_col << "]";
        if(!child.m_nodes.empty())
        {            
            printGraph(child);            
        }
        cout << endl;
    }

}

void printGraph(const Node &node)
{
    using namespace std;

    for(auto child : node.m_nodes)
    {
        cout << "[ " << child.m_row << ", " << child.m_col << "]";
        printGraph(child);
    }
}

void printIslands(const std::vector<std::vector<int>> &data)
{
    using namespace std;
    for(auto row : data)
    {
        for(auto col : row)
        {
            cout << col << " ";
        }
        cout << endl;
    }
}

void removeIslands(std::vector<std::vector<int>> data)
{
    using namespace std;
    printIslands(data);

    Node graph;
    createGraph(data, graph);
    //printGraphHead(graph);    
    
    set<pair<int, int>> islandsToKeep;
    islandsToKeepFromGraph(islandsToKeep, graph);

    for(int i = 0; i < data.size(); i++)
    {
        std::vector<int> &cols = data[i];
        for(int j = 0; j < cols.size(); j++)
        {
            if(cols[j] == 1)
            {
                pair<int,int> currentIsland(i, j);

                if(islandsToKeep.find(currentIsland) == islandsToKeep.end())
                {
                    cols[j] = 0;
                }
            }
        }
    }
    cout << "***************" << endl;

    printIslands(data);
    //printGraphHead(graph);
}


// Driver Code
int main()
{
    /*
    Remove all the 1s that are not connected to the border of the matrix.
    Items are considered connected if you can reach them vertically or horizontally - 
    diagonal is not connected (the top left 1 and the 1 to the right beneath it are NOT connected for example)

        Input:
        [
        [1, 0, 0, 0, 0, 0]
        [0, 1, 0, 1, 1, 1]
        [0, 0, 1, 0, 1, 0]
        [1, 1, 0, 0, 1, 0]
        [1, 0, 1, 1, 0, 0]
        [1, 0, 0, 0, 0, 1]
        ]

        Example output:
        [
        [1, 0, 0, 0, 0, 0]
        [0, 0, 0, 1, 1, 1]
        [0, 0, 0, 0, 1, 0]
        [1, 1, 0, 0, 1, 0]
        [1, 0, 0, 0, 0, 0]
        [1, 0, 0, 0, 0, 1]
        ]

        Islands removed:
        [
        [ ,  ,  ,  ,  , ]
        [ , 1,  ,  ,  , ]
        [ ,  , 1,  ,  , ]
        [ ,  ,  ,  ,  , ]
        [ ,  , 1, 1,  , ]
        [ ,  ,  ,  ,  , ]
        ]

    */

   using namespace std;

   vector<vector<int>> data = {
        {1, 0, 0, 0, 0, 0},
        {0, 1, 0, 1, 1, 1},
        {0, 0, 1, 0, 1, 0},
        {1, 1, 0, 0, 1, 0},
        {1, 0, 1, 1, 0, 0},
        {1, 0, 0, 0, 0, 1}
   };

    removeIslands(data);
}